"""Test the command line options for the scrips."""
from copy import deepcopy
import importlib
import logging
import random
import pytest


def ris():
  """Return random int as string."""
  return str(random.randint(0, 100000))


def fis():
  """Return foo plus random int as string."""
  return 'foo' + str(random.randint(0, 100000))


SCRIPTS = [('ILCDIRAC.Core.scripts.dirac-ilc-add-cvmfs-software', {}),
           ('ILCDIRAC.Core.scripts.dirac-ilc-add-software', {}),
           ('ILCDIRAC.Core.scripts.dirac-ilc-add-user', {'Email': {'S': ['foo@bar.baz'], 'E': [fis()]},
                                                         'VO': {'S': ['calice'], 'E': ['vo']}}),
           ('ILCDIRAC.Core.scripts.dirac-ilc-add-whizard', {}),
           ('ILCDIRAC.Core.scripts.dirac-ilc-list-users', {'VO': {'S': ['calice'], 'E': ['vo']}}),
           ('ILCDIRAC.ILCTransformationSystem.scripts.dirac-clic-make-productions', {'configFile': {'E': ['any'],
                                                                                                    'S': []}}),
           ('ILCDIRAC.ILCTransformationSystem.scripts.dirac-ilc-add-tasks-to-prod', {'Tasks':
                                                                                     {'E': [fis()], 'S': [ris()]},
                                                                                     'ProductionID':
                                                                                     {'E': [fis()], 'S': [ris()]}}),
           ('ILCDIRAC.ILCTransformationSystem.scripts.dirac-ilc-filestatus-transformation', {}),
           ('ILCDIRAC.ILCTransformationSystem.scripts.dirac-ilc-get-info', {'ProductionID':
                                                                            {'E': [fis()], 'S': [ris()]}}),
           ('ILCDIRAC.ILCTransformationSystem.scripts.dirac-ilc-get-prod-log',
            {'Query': {'S': ['Foo:Bar'], 'E': ['FooBar']}}),
           #  ('ILCDIRAC.ILCTransformationSystem.scripts.dirac-ilc-make-ddsimtarball', {}),
           ('ILCDIRAC.ILCTransformationSystem.scripts.dirac-ilc-moving-transformation', {'ps': True}),
           ('ILCDIRAC.ILCTransformationSystem.scripts.dirac-ilc-production-summary', {'prods':
                                                                                      {'E': [fis()], 'S': [ris()]},
                                                                                      'sample_size':
                                                                                      {'E': [fis()], 'S': [ris()]}}),
           ('ILCDIRAC.ILCTransformationSystem.scripts.dirac-ilc-replication-transformation', {'ps': True}),
           ('ILCDIRAC.ILCTransformationSystem.scripts.dirac-ilc-upload-gen-files', {'Energy':
                                                                                    {'E': [fis()], 'S': [ris()]},
                                                                                    'EvtID':
                                                                                    {'E': [fis()], 'S': [ris()]},
                                                                                    'NumberOfEvents':
                                                                                    {'E': [fis()], 'S': [ris()]},
                                                                                    'XSectionError':
                                                                                    {'E': [fis()], 'S': [ris()]},
                                                                                    'XSection':
                                                                                    {'E': [fis()], 'S': [ris()]},
                                                                                    'Luminosity':
                                                                                    {'E': [fis()], 'S': [ris()]}}),
           ('ILCDIRAC.ILCTransformationSystem.scripts.dirac-transformation-recover-data', {'ProdID':
                                                                                           {'E': [fis()], 'S': [ris()]},
                                                                                           }),
           ('ILCDIRAC.Interfaces.scripts.dirac-ilc-find-in-FC', {}),
           ('ILCDIRAC.Interfaces.scripts.dirac-ilc-show-software', {}),
           ('ILCDIRAC.Interfaces.scripts.dirac-repo-create-lfn-list', {}),
           ('ILCDIRAC.Interfaces.scripts.dirac-repo-retrieve-jobs-output-data', {}),
           ('ILCDIRAC.Interfaces.scripts.dirac-repo-retrieve-jobs-output', {}),
           ('ILCDIRAC.Interfaces.scripts.TestAndProbeSites', {}),
           ]


@pytest.mark.parametrize('scriptPath, options', SCRIPTS)
def test_scriptCLI(caplog, scriptPath, options, mocker):
  """Check that calling the callback does something."""
  caplog.set_level(logging.DEBUG)
  rsm = mocker.Mock()
  mocker.patch('DIRAC.Core.Base.Script.registerSwitch', new=rsm)
  try:
    module = importlib.import_module(scriptPath)
  except ImportError:
    assert False
  try:
    paramClass = getattr(module, '_Params')
    theParams = paramClass()
    if options.get('ps'):
      theParams.registerSwitches(rsm)
    else:
      theParams.registerSwitches()
  except AttributeError as e:
    logging.info(dir(module))
    assert False, 'ERROR: ' + str(e)
    return theParams

  for call in rsm.call_args_list:
    logging.info(call)
    flag = call[0][1].strip('=:')
    callback = call[0][3]
    if isinstance(callback, bool):
      continue
    paramBefor = deepcopy(vars(theParams))
    errorFlags = options.get(flag, {}).get('E', [])
    successFlags = options.get(flag, {}).get('S', [ris(), fis()])
    for value in errorFlags:
      retVal = callback(value)
      assert not retVal['OK'], 'Value should cause failure'

    for value in successFlags:
      retVal = callback(value)
      if not retVal['OK']:
        assert False, 'failure for ' + value + flag + str(successFlags) + str(options)
        continue

      paramAfter = vars(theParams)
      logging.info('befor: %s', sorted(paramBefor))
      logging.info('after: %s', sorted(paramAfter))
      logging.info('befor: %s', paramBefor.values())
      logging.info('after: %s', paramAfter.values())
      assert paramBefor != paramAfter
      # assert any(randVal in aVal for aVal in paramAfter.values() if aVal)
      diff = 0
      for name, befor in paramBefor.items():
        after = paramAfter[name]
        logging.info('Befor: %r -- After: %r', befor, after)
        if after == value:
          logging.info('The Rand Val: %r', value)
          diff = 1
          break
        if befor != after:
          logging.info('Difference')
          diff += 1
      assert diff == 1
