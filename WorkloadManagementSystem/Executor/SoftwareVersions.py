"""Executor to check which Sites have the proper software installed for any given job.

Based on some part of the SoftwarePackage name we ban a lists of sites.

Arbitrary number of BanLists can be created. In the CS: BanLists is a list of strings, for each string create two more
options.

* <string>Reason
* <string>Sites

Where Reason is the substring of the softwarePackage that is looked for and Sites is a lists of sites to be banned if
the software package includes the substring

Example:

+----------------------------+--------------------------------------------+---------------------------------------+
|  **Option**                |    **Description**                         |  **Example**                          |
+----------------------------+--------------------------------------------+---------------------------------------+
| BanLists                   | List of Reason/Sites combinations          | BanLists=CVMFS                        |
|                            |                                            |                                       |
+----------------------------+--------------------------------------------+---------------------------------------+
| CVMFSReason                | String matched in software version names   | CVMFSReason=ILCSoft                   |
+----------------------------+--------------------------------------------+---------------------------------------+
| CVMFSSites                 | Sites added to the ban lists of the        | CVMFSSites=LCG.SomeSite.cern          |
|                            | software version matches                   |                                       |
+----------------------------+--------------------------------------------+---------------------------------------+

"""

__RCSID__ = '$Id$'

import time
from pprint import pformat

from DIRAC import S_OK
from DIRAC.WorkloadManagementSystem.Executor.Base.OptimizerExecutor import OptimizerExecutor


class SoftwareVersions(OptimizerExecutor):
  """Executor Class for Auto Banning based on software use."""

  @classmethod
  def initializeOptimizer(cls):
    """Initialize specific parameters for SoftwareVersions."""
    cls.__softToBanned = {}
    cls.__lastCacheUpdate = 0
    cls.__cacheLifeTime = 600
    return S_OK()

  def _updateBanLists(self):
    """Update the list of banned sites and reasons."""
    banLists = self.ex_getOption('BanLists', [])
    self.log.notice(pformat(banLists))

    for banList in banLists:
      resReason = self.ex_getOption(banList + 'Reason', '')
      resSites = self.ex_getOption(banList + 'Sites', [])
      self.__softToBanned[resReason] = resSites

    self.log.notice('BanLists:', pformat(self.__softToBanned))

    return S_OK()

  def optimizeJob(self, jid, jobState):
    """Update the banlists if pattern matched."""
    now = time.time()
    if (now - self.__lastCacheUpdate) > self.__cacheLifeTime:
      self.__lastCacheUpdate = now
      self._updateBanLists()

    result = jobState.getManifest()
    if not result['OK']:
      return result
    manifest = result['Value']

    software = manifest.getOption('SoftwarePackages')
    self.log.verbose('SoftwarePackages: %s ' % software)
    if isinstance(software, basestring):
      software = [software]

    if software:
      self.checkSoftware(manifest, software)

    result = jobState.setStatus('SoftwareCheck',
                                'Done',
                                appStatus='',
                                source=self.ex_optimizerName())
    if not result['OK']:
      return result

    self.log.verbose('Done SoftwareVersioning')

    return self.setNextOptimizer(jobState)

  def checkSoftware(self, manifest, software):
    """Check Manifest and update BannedSites.

    Check if there are softwarepackages needed for the job and ban all sites if there is some prohibitions for that
    package.
    """
    bannedSites = manifest.getOption('BannedSites', [])
    if not bannedSites:
      bannedSites = manifest.getOption('BannedSite', [])

    self.log.verbose('Original BannedSites:', bannedSites)

    softBanned = set()
    for reason, sites in self.__softToBanned.iteritems():
      for package in software:
        self.log.verbose('Checking %s against %s ' % (reason, package))
        if reason in package:
          softBanned.update(sites)

    newBannedSites = sorted(set(bannedSites).union(softBanned))
    newBannedSites = ', '.join(newBannedSites)
    manifest.setOption('BannedSites', newBannedSites)

    self.log.notice('Updated BannedSites', newBannedSites)
    return
