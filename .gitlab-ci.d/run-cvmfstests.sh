#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source .gitlab-ci.d/set-reportstyle.sh
export COVERAGE_PROCESS_START=$DIR/../.coveragerc
export PYTEST_ADDOPTS="${PYTEST_ADDOPTS} --cov-append --junitxml=junit_job.xml"
pytest -n 2  --randomly-dont-reorganize $DIR/../Tests/Integration/Test_Jobs.py
testResult=$?
coverage combine --append tmp*/*/.coverage*
coverage report -i
coverage xml -i -o coverage.xml
coverage html -i
sed -i "s@installation/ILCDIRAC/@@g" coverage.xml
exit ${testResult}
