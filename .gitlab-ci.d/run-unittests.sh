#!/bin/bash
source .gitlab-ci.d/set-reportstyle.sh
export PYTEST_ADDOPTS=$PYTEST_ADDOPTS" -m 'not integration' --junitxml=junit_unit.xml"
pytest
testResult=$?
sed -i "s@installation/ILCDIRAC/@@g" coverage.xml
exit ${testResult}
